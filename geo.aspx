<%@ Page Language="C#" AutoEventWireup="true" EnableSessionState="False" EnableViewState="False" EnableViewStateMac="False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>Header Dump</title>
</head>
<body>
<form id="formHeaderDump" runat="server">
</form>
</body>
</html>
<script runat="server">
void Page_Load(object sender, EventArgs e)
{
	int loop1, loop2;
	NameValueCollection coll;

	// Load Header collection into NameValueCollection object.
	coll=Request.Headers;

	// Put the names of all keys into a string array.
	String[] arr1 = coll.AllKeys; 
	for (loop1 = 0; loop1<arr1.Length; loop1++) 
	{
	   Response.Write("Key: " + arr1[loop1] + "<br>");
	   // Get all values under this key.
	   String[] arr2=coll.GetValues(arr1[loop1]);
	   for (loop2 = 0; loop2<arr2.Length; loop2++) 
	   {
	      Response.Write("Value " + loop2 + ": " + Server.HtmlEncode(arr2[loop2]) + "<br>");
	   }
	}
}
</script>